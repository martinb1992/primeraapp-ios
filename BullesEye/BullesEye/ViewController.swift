//
//  ViewController.swift
//  BullesEye
//
//  Created by Martin Balarezo on 24/4/18.
//  Copyright © 2018 Martin Balarezo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    

   
    
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var roundLabel: UILabel!
    @IBOutlet weak var cheatLabel: UILabel!
   @IBOutlet weak var gameSlider: UISlider!
    
    /*var target=0
    var score = 0
    var roundGame = 0*/
    let gameModel = Game()
    
    var cheat = 0
    
    
    @IBAction func aboutButton(_ sender: Any) {
        
    }
    
    
    
    @IBAction func GGButton(_ sender: Any) {
        
       /* if score > 100 {
            performSegue(withIdentifier: "idViewSegue", sender: self)
        }
      */
        
        if gameModel.score > 100 {
            performSegue(withIdentifier: "idViewSegue", sender: self)
        }
        
    }
    
    @IBAction func playButton(_ sender: Any) {
        let sliderValue = Int(gameSlider.value)
        gameModel.play(sliderValue: sliderValue)
        /*let sliderValue=Int(gameSlider.value)
        
        switch sliderValue {
        case target:
            score += 100
        case (target-2)...(target+2):
            score += 50
        case (target-5)...(target+5):
            score += 10
        default:
            break
        }
        
        roundGame+=1
        target=Int(arc4random_uniform(100))
        
        scoreLabel.text = "\(score)"
        targetLabel.text = "\(target)"
        roundLabel.text = "\(roundGame)"*/
    }
    /*
    @IBAction func RestartButton(_ sender: Any) {
        restartGame()
    }*/
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       // restartGame()
        
        gameModel.restartGame()
        setValues()
        
    }
 
 
    
    @IBAction func viewSliderValue(_ sender: Any) {
        cheatLabel.text = "\(Int(gameSlider.value))"
    }
    
    func setValues(){
        targetLabel.text = "\(gameModel.target)"
        scoreLabel.text = "\(gameModel.score)"
        roundLabel.text = "\(gameModel.roundGame)"
    }
    
    /*
    private func restartGame()
    {
        target = Int(arc4random_uniform(100))
        scoreLabel.text = "0"
        targetLabel.text = "\(arc4random_uniform(100))"
        score=0
        roundLabel.text="1"
    }*/
    
}

